using System.ComponentModel.DataAnnotations;

namespace Avico.DTO
{
    /// <summary>
    /// Contact DTO
    /// </summary>
    public class Telephone
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int NumeroTelephone { get; set; }
        [Required]
        public int ContactId;
        public Contacts Contacts;
    }
}